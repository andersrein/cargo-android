use std::collections::HashMap;
use std::path::PathBuf;
use std::env;
use std::fs::{File, create_dir_all};
use std::io::{BufReader, Write};
use std::process::Command;

use log::{error, warn, info, debug};
use regex::Regex;
use serde::{Serialize, Deserialize};

mod config;

use config::Config;

type Params = HashMap<String, String>;

#[derive(Debug, PartialEq, Eq)]
enum ExpandError {
    MissingKey(String)
}

fn expand(template: &str, params: &Params) -> Result<String, ExpandError> {
    let re = Regex::new(r"(?:[^%]|^)[%]([^%]+)[%](?:[^%]|$)").unwrap();

    let mut current = template.to_string();
    while let Some(cap) = re.captures(&current) {
        let m = cap.get(1).unwrap();
        let key = m.as_str();

        if let Some(value) = params.get(key) {
            let trailer = current[m.end()+1..].to_string();
            current = current[0..m.start()-1].to_string();
            current.push_str(&value);
            current.push_str(&trailer);
        } else {
            return Err(ExpandError::MissingKey(key.to_string()));
        }
    }

    Ok(current)
}

#[test]
fn test_expand()
{
    let mut params = Params::new();
    params.insert("foo".to_string(), "hello".to_string());
    params.insert("bar".to_string(), "goodbye".to_string());

    assert_eq!(expand("%foo%", &params).unwrap(), "hello");
    assert_eq!(expand("%bar%", &params).unwrap(), "goodbye");
    assert_eq!(expand("%bar%", &params).unwrap(), "goodbye");
    assert_eq!(
        expand("%baz%", &params),
        Err(ExpandError::MissingKey("baz".to_string()))
    );
    assert_eq!(expand("%foo% lol %bar%", &params).unwrap(), "hello lol goodbye");
    assert_eq!(expand("%foo% lol %foo%", &params).unwrap(), "hello lol hello");
}

fn write_and_expand(template: &str, params: &Params, path: PathBuf) {
    let expanded = expand(template, params).unwrap();

    create_dir_all(path.parent().unwrap()).unwrap();

    debug!("Writing file: {:?}", path);
    let mut file = File::create(path.to_str().unwrap()).unwrap();
    write!(file, "{}", expanded).unwrap();
}

fn write_bytes(bytes: &[u8], path: PathBuf) {
    let mut file = File::create(path.to_str().unwrap()).unwrap();
    file.write(bytes).unwrap();
}

fn generate_project(config: &Config) {
    let manifest =
        if config.oculus {
            String::from_utf8(include_bytes!("../templates/AndroidManifestOculus.xml").to_vec()).unwrap()
        } else {
            String::from_utf8(include_bytes!("../templates/AndroidManifest.xml").to_vec()).unwrap()
        };
    let build = String::from_utf8(include_bytes!("../templates/build.gradle").to_vec()).unwrap();
    let signing_template = String::from_utf8(include_bytes!("../templates/signing.gradle").to_vec()).unwrap();
    let signing_ref_template = String::from_utf8(include_bytes!("../templates/signing_ref.gradle").to_vec()).unwrap();

    let mut params = Params::new();

    if let Some(ref signing) = config.signing {
        params.insert("key_alias".to_string(), signing.key_alias.clone());
        params.insert("key_password".to_string(), signing.key_password.clone());

        
        params.insert("store_file".to_string(), signing.store_file.clone());
        params.insert("store_password".to_string(), signing.store_password.clone());
        params.insert(
            "signing_configs".to_string(),
            expand(&signing_template, &params).unwrap()
        );
        params.insert(
            "signing_config_ref".to_string(),
            expand(&signing_ref_template, &params).unwrap()
        );

    } else {
        params.insert("signing_configs".to_string(), String::new());
        params.insert("signing_config_ref".to_string(), String::new());
    }

    params.insert("package".to_string(), config.package.clone());
    params.insert("label".to_string(), config.label.clone());
    params.insert("library_name".to_string(), config.library_name.clone());

    let escaped_abi_list : Vec<String> =
        config.abi_list.iter().map(|x| format!("'{}'", x)).collect();

    params.insert("abi_list".to_string(), escaped_abi_list.join(", "));
    params.insert("api_level".to_string(), format!("{}", config.api_level));

    let project_dir = config.project_dir.clone();

    /* Write project files */
    write_and_expand(
        &manifest,
        &params,
        project_dir.join("AndroidManifest.xml")
    );
    write_and_expand(
        &build,
        &params,
        project_dir.join("build.gradle")
    );

    /* Write gradle files */
    let gradlew = include_bytes!("../resources/gradlew");
    let gradlew_bat = include_bytes!("../resources/gradlew.bat");
    let gradle_properties = include_bytes!("../resources/gradle-wrapper.properties");
    let gradle_jar = include_bytes!("../resources/gradle-wrapper.jar");

    write_bytes(gradlew, project_dir.join("gradlew"));
    write_bytes(gradlew_bat, project_dir.join("gradlew.bat"));

    let gradle_wrapper_path = project_dir.join("gradle").join("wrapper");
    create_dir_all(gradle_wrapper_path.to_str().unwrap()).unwrap();
    write_bytes(gradle_properties, gradle_wrapper_path.join("gradle-wrapper.properties"));
    write_bytes(gradle_jar, gradle_wrapper_path.join("gradle-wrapper.jar"));
}

#[derive(Serialize, Deserialize)]
struct Args {
    pub flag_build: bool,
    pub flag_install: bool,
    pub flag_run: bool,
    pub flag_release: bool,
}

static USAGE: &'static str = r#"
Usage:
    cargo android [--install] [--build] [--run] [--release]

Options:
    --build                 Build the generated Android project
    --install               Build & install the generated Android project onto the connected device
    --run                   Build, install and run the generated Android project on the connected device
    --release               Build release build (Debug build otherwise)

This command allows you to generate a Android project from this rust project.
"#;

fn abi_to_target(abi: &str) -> String {
    let mut m = HashMap::new();

    m.insert("arm64-v8a", "aarch64-linux-android");
    m.insert("armeabi-v7a", "armv7-linux-androideabi");
    m.insert("x86", "i686-linux-android");
    m.insert("x86_64", "x86_64-linux-android");

    if let Some(target) = m.get(abi) {
        target.to_string()
    } else {
        panic!(
            "Invalid ABI: {}. Possible values: {:?}",
            abi, m.keys()
        );
    }
}

fn get_ndk_home() -> PathBuf {
    std::env::var("NDK_HOME")
        .expect("NDK_HOME not set!")
        .into()
}

fn get_ovr_sdk_mobile_home() -> PathBuf {
    std::env::var("OVR_SDK_MOBILE_HOME")
        .expect("OVR_SDK_MOBILE_HOME not set!")
        .into()
}

fn validate_ndk() {
    let ndk_home = get_ndk_home();

    let mut source_properties_path = ndk_home.clone();
    source_properties_path.push("source.properties");

    debug!("Opening {:?}", source_properties_path);

    let source_properties_file = File::open(source_properties_path.clone())
        .expect(
            &format!(
                "Failed to open {:?}. Is NDK_HOME set correctly?",
                source_properties_path
            )
        );

    let source_properties_reader = BufReader::new(source_properties_file);

    let source_properties : HashMap<String, String> =
        java_properties::read(source_properties_reader).unwrap();

    let description = source_properties.get("Pkg.Desc")
        .expect("NDK source.properties is missing Pkg.Desc field");

    let expected_description = "Android NDK";

    if description != expected_description {
        panic!(
            "Found {:?}, but it does not appear to be NDK. Excepted Pkg.Desc \
             to be {:?}, but it was {:?}",
            source_properties_path, expected_description, description
        );
    }

    let revision = source_properties.get("Pkg.Revision")
        .expect("NDK source.properties is missing Pkg.Revision field");

    let major : u32 = revision.split(".").next().unwrap().parse()
        .expect("Failed to parse Pkg.Revision");

    let minimal = 19;

    if major < minimal {
        panic!("The current NDK version, {}, is outdated. cargo-android only supports {} or higher", revision, minimal);
    }

    debug!("NDK validation OK. Revision: {}", revision);
}

#[test]
fn test_validate_ndk() {
    pretty_env_logger::init();

    validate_ndk();
}

fn get_ndk_sysroot() -> PathBuf {
    let ndk_home = get_ndk_home();

    // <NDK_HOME>/toolchains/llvm/prebuilt/linux-x86_64/sysroot

    let mut sysroot = ndk_home.clone();
    sysroot.push("toolchains");
    sysroot.push("llvm");
    sysroot.push("prebuilt");
    sysroot.push("linux-x86_64"); // TODO: Other host platforms
    sysroot.push("sysroot");

    if !sysroot.exists() {
        panic!(
            "Android NDK sysroot derived from NDK_HOME variable does not exist \
            ({:?})", sysroot
        );
    }

    sysroot
}

fn find_adb() -> PathBuf {
    if let Ok(android_home) = std::env::var("ANDROID_HOME") {
        let mut path : PathBuf = android_home.into();
        path.push("platform-tools");
        path.push("adb");
        if !path.exists() {
            panic!("adb not found at {:?}", path);
        }
        path
    } else {
        panic!("ANDROID_HOME is not set");
    }
}

#[test]
fn test_get_ndk_sysroot() {
    println!("sys_root={:?}", get_ndk_sysroot());
}

fn get_libcpp_path(target: &str) -> PathBuf {
    // <SYSROOT>/usr/lib/aarch64-linux-android/libc++_shared.so
    let mut libcpp = get_ndk_sysroot();
    libcpp.push("usr");
    libcpp.push("lib");
    libcpp.push(target);
    libcpp.push("libc++_shared.so");

    if !libcpp.exists() {
        panic!("Expected to find libc at {:?}", libcpp);
    }

    libcpp
}

fn setup_cross_compile_environment(target: &str, android_api_level: u32) {

    let sysroot = get_ndk_sysroot();

    let mut llvm_config = sysroot.clone();
    llvm_config.pop();
    llvm_config.push("bin");
    llvm_config.push("llvm-config");

    let mut cc_target = target.to_string();

    // The rust target does not match the expected triplet by clang
    if cc_target == "armv7-linux-androideabi" {
        cc_target = "armv7a-linux-androideabi".to_string();
    }

    cc_target = format!("{}{}", cc_target, android_api_level);

    let c_compiler_base_name =
        format!("{}-clang", cc_target);

    let mut c_compiler = sysroot.clone();
    c_compiler.pop();
    c_compiler.push("bin");
    c_compiler.push(&c_compiler_base_name);

    if !c_compiler.exists() {
        panic!("Expected to find C compiler at: {:?}", c_compiler);
    }

    let cxx_compiler_base_name =
        format!("{}-clang++", cc_target);

    let mut cxx_compiler = sysroot.clone();
    cxx_compiler.pop();
    cxx_compiler.push("bin");
    cxx_compiler.push(&cxx_compiler_base_name);

    if !cxx_compiler.exists() {
        panic!("Expected to find C compiler at: {:?}", cxx_compiler);
    }

    let upper_case_target : String = target.to_uppercase().chars()
        .map(|c| if c == '-' { '_' } else { c })
        .collect();

    let set_var_debug = |var: &str, value: &str| {
        debug!("Setting environment variable: {}={}", var, value);
        std::env::set_var(var, value);
    };

    let linker_var = format!("CARGO_TARGET_{}_LINKER", upper_case_target);
    // This is all that is required for compiling pure rust code...
    set_var_debug(&linker_var, c_compiler.to_str().unwrap());

    // ... however when the rust projects are building C/C++ dependencies
    // these variables must be set.
    set_var_debug("CC", c_compiler.to_str().unwrap());
    set_var_debug("CXX", cxx_compiler.to_str().unwrap());
    set_var_debug("CC_TARGET", &cc_target);

    // ... also in order for the correct headers to be used when generating rust
    // bindings we need to pass this argument
    set_var_debug("BINDGEN_EXTRA_CLANG_ARGS", &format!("--sysroot={} --target={}", sysroot.to_str().unwrap(), cc_target));
    set_var_debug("ANDROID_SYSROOT", sysroot.to_str().unwrap());
    set_var_debug("LLVM_CONFIG_PATH", llvm_config.to_str().unwrap());
}


#[test]
fn test_setup_cross_compile_environment()
{
    pretty_env_logger::init();

    for target in &["aarch64-linux-android", "armv7a-linux-androideabi", "i686-linux-android"] {
        setup_cross_compile_environment(target, 24);

        for var in &["ANDROID_API_LEVEL", "CC", "CXX", "BINDGEN_EXTRA_CLANG_ARGS", "ANDROID_SYSROOT"] {
            println!("{}={}", var, std::env::var(var).unwrap());
        }
    }
}

fn main() {
    pretty_env_logger::init();

    validate_ndk();

    let args = docopt::Docopt::new(USAGE)
        .and_then(|d| d.deserialize::<Args>())
        .unwrap_or_else(|err| err.exit());

    let manifest =
        if let Ok(from_env) = env::var("CARGO_MANIFEST_DIR") {
            PathBuf::from(from_env).join("Cargo.toml")
        } else {
            env::current_dir().unwrap().join("Cargo.toml")
        };

    let config = Config::new(manifest);

    if args.flag_install && args.flag_release && config.signing.is_none() {
        panic!(
            "Cannot install release build without the \
            [package.metadata.android.signing] section available in Cargo.toml"
        );
    }

    generate_project(&config);

    for abi in config.abi_list.iter() {
        let target = abi_to_target(abi);

        info!("Building native code for ABI: {}", abi);
        if abi == "armeabi-v7a" {
            warn!(
                "armeabi-v7a might fail when used in combination with bindgen \
                for functions that use the aapcs-vfp calling convertions"
            );
        }

        setup_cross_compile_environment(&target, config.api_level);

        let mut cargo_args = vec!["build", "--target", &target, "--lib"];

        if args.flag_release {
            cargo_args.push("--release");
        }

        debug!("Running cargo {}", cargo_args.join(" "));

        let success = Command::new("cargo")
            .args(&cargo_args)
            .status()
            .unwrap()
            .success();

        if ! success {
            error!("Failed to build native library for abi: {}", abi);
            std::process::exit(1);
        }

        let lib_path = config.target_dir
            .join(&target)
            .join(if args.flag_release { "release" } else { "debug" })
            .join(format!("lib{}.so", &config.library_name));

        if !lib_path.exists() {
            panic!("Expected to find {:?} after native build completed", lib_path);
        }

        let libs_dir = config.project_dir
            .join("libs")
            .join(abi);

        create_dir_all(&libs_dir).unwrap();



        // TODO: Bundling libc++ should be optional since not all applications require it.
        if config.bundle_cpp_runtime {
            let src = get_libcpp_path(&target);
            debug!("Bundling libc++ ({:?})", src);

            let target = libs_dir.join(src.file_name().unwrap());
            std::fs::copy(src.clone(), target).unwrap();
        }

        if config.oculus {
            let mut libvrapi = get_ovr_sdk_mobile_home();
            libvrapi.push("VrApi");
            libvrapi.push("Libs");
            libvrapi.push("Android");
            libvrapi.push(&abi);
            // TODO: Should we actually use the Debug library
            // for debug builds? 
            libvrapi.push("Release");
            libvrapi.push("libvrapi.so");

            let target = libs_dir.join("libvrapi.so");
            std::fs::copy(libvrapi, target).unwrap();
        }

        let mut expand_vars = HashMap::new();
        for (key, value) in env::vars() {
            expand_vars.insert(
                key.as_str().to_string(),
                value.as_str().to_string(),
            );
        }
        expand_vars.insert("ABI".to_string(), abi.to_string());

        for bundle_lib in &config.bundle_libs {
            let expanded_path : PathBuf =
                match expand(bundle_lib, &expand_vars) {
                    Ok(expanded) => { expanded.into() },
                    Err(ExpandError::MissingKey(key)) => {
                        panic!(
                            "bundle_lib template {:?} uses \
                            environment variable {:?} but it is \
                            not set",
                            bundle_lib,
                            key,
                        );
                    }
                };

            if !expanded_path.exists() {
                panic!(
                    "Template {:?} expanded to {:?} which does \
                    not exist",
                    bundle_lib,
                    expanded_path
                );
            }


            let target =
                libs_dir.join(expanded_path.file_name().unwrap());

            info!("Bundling library {:?}", expanded_path);

            std::fs::copy(expanded_path, target).unwrap();
        }

        if config.bundle_vulkan_layers {
            let vulkan_layers_dir = get_ndk_home()
                .join("sources")
                .join("third_party")
                .join("vulkan")
                .join("src")
                .join("build-android")
                .join("jniLibs")
                .join(abi);

            let layers = std::fs::read_dir(&vulkan_layers_dir)
                .expect("Failed to read vulkan layers dir");


            for layer in layers {
                let layer = layer.unwrap();
                let target = libs_dir.join(layer.file_name());
                std::fs::copy(layer.path(), target).unwrap();
            }
        }

        let lib_name =
            format!("lib{}.so", &config.library_name);

        std::fs::copy(lib_path, libs_dir.join(lib_name)).unwrap();
    }

    let task_suffix = if args.flag_release { "Release" } else { "Debug" };

    let task =
        if args.flag_install {
            Some(format!("install{}", task_suffix))
        } else if args.flag_build {
            Some(format!("assemble{}", task_suffix))
        } else {
            None
        };

    if let Some(task) = task {
        info!("Triggering Gradle task \"{}\"", task);

        let cmd = vec!["gradlew", &task];

        let success = Command::new("bash")
            .args(&cmd)
            .current_dir(config.project_dir)
            .status()
            .expect(&format!("Failed to run bash with arguments: {:?}", cmd))
            .success();

        if !success {
            error!("Gradle command failed: {:?}", cmd);
            std::process::exit(1);
        }
    }

    if args.flag_run {
        let adb = find_adb();
        info!("adb found at {:?}", adb);

        let package = &config.package;
        let activity = "android.app.NativeActivity";
        let activity_path = format!("{}/{}", package, activity);

        let mut cmd = vec!["shell", "am", "start", "-n"];
        cmd.push(&activity_path);

        let success = Command::new(adb)
            .args(&cmd)
            .status()
            .expect(&format!("Failed to run adb with arguments: {:?}", cmd))
            .success();

        if !success {
            error!("adb command failed: {:?}", cmd);
            std::process::exit(1);
        }

        info!("Application started");
    }

}
