use std::path::PathBuf;
use std::fs::File;
use std::io::Read;

use serde::Deserialize;

#[derive(Deserialize, Clone)]
pub struct Signing {
    pub key_alias: String,
    pub key_password: String,
    pub store_file: String,
    pub store_password: String
}

impl Signing {
    fn fix_path(mut self, manifest: &PathBuf) -> Signing {
        let store_file : PathBuf = self.store_file.clone().into();
        if store_file.is_relative() {
            let mut actual_path = manifest.clone();
            actual_path.pop();
            actual_path.push(store_file);
            self.store_file = actual_path.to_str().unwrap().to_string();
        }
        self
    }
}

#[derive(Deserialize, Clone)]
struct AndroidToml {
    package: Option<String>,
    label: Option<String>,
    abi_list: Option<Vec<String>>,
    api_level: Option<u32>,
    oculus: Option<bool>,
    bundle_cpp_runtime: Option<bool>,
    bundle_vulkan_layers: Option<bool>,
    signing: Option<Signing>,
    bundle_libs: Option<Vec<String>>,
}

#[derive(Deserialize, Clone)]
struct MetadataToml {
    android: Option<AndroidToml>
}

#[derive(Deserialize, Clone)]
struct PackageToml {
    metadata: Option<MetadataToml>
}

#[derive(Deserialize, Clone)]
struct LibToml {
    name: String
}

#[derive(Deserialize, Clone)]
struct CargoToml {
    package: PackageToml,
    lib: Option<LibToml>,
}

pub struct Config {
    pub library_name: String,
    pub api_level: u32,
    pub package: String,
    pub label: String,
    pub abi_list: Vec<String>,
    pub target_dir: PathBuf,
    pub project_dir: PathBuf,
    pub signing: Option<Signing>,
    pub oculus: bool,
    pub bundle_cpp_runtime: bool,
    pub bundle_vulkan_layers: bool,
    pub bundle_libs: Vec<String>,
}

impl Config {
    pub fn new(manifest: PathBuf) -> Config {
        let content = {
            let mut file = File::open(&manifest).unwrap();
            let mut content = String::new();
            file.read_to_string(&mut content).unwrap();
            content
        };

        let cargo_toml: CargoToml = toml::from_str(&content).unwrap();

        let metadata = cargo_toml.package.metadata
            .expect("Missing [package.metadata] section")
            .clone();

        let android = metadata.android
            .expect("Missing [package.metadata.android] section")
            .clone();

        let target_dir = manifest.parent().unwrap().join("target");

        Config {
            library_name: cargo_toml.lib
                .map(|lib| lib.name)
                .expect("Missing name from [lib] section"),
            package: android.package
                .expect("Missing package field in [package.metadata.android]"),
            label: android.label
                .expect("Missing label field in [package.metadata.android]"),
            abi_list: android.abi_list
                .expect("Missing abi_list field in [package.metadata.android]"),
            api_level: android.api_level
                .expect("Missing api_level field in [package.metadata.android]"),
            signing: android.signing.map(|s| s.fix_path(&manifest)),
            target_dir: target_dir.clone(),
            project_dir: target_dir.join("android-project"),
            oculus: android.oculus.unwrap_or(false),
            bundle_cpp_runtime:
                android.bundle_cpp_runtime.unwrap_or(false),
            bundle_vulkan_layers:
                android.bundle_vulkan_layers.unwrap_or(false),
            bundle_libs: android.bundle_libs.unwrap_or(Vec::new()),
        }
    }
}

