In order to setup a project to be cross platform compatible with
Android you need the following entries in the Cargo.toml file of
your project:

[lib]
name = "<shared library name>"
crate-type = ["cdylib"]

[[bin]]
name = "<desktop version executable name>"
path = "src/lib.rs"

[package.metadata.android]
package = "<java package name>"
label = "<User visible App name>"
abi_list = ["arm64-v8a"]
api_level = 26

Short description of the fields:

    <shared library name>

        The name of the shared library without "lib" prefix and
        ".so" suffix. This doesn't affect anything but the content
        of the APK. 

    <desktop version executable name>

        This is the name of the exectuable that you will run for
        desktop platformss

    <java package name>

        Typical java package name. For instance "com.mycompany.foo"

    <label>

        This is the name of the app that appears in list of Apps
        on the users phone.

The abi_list can be extended to multiple targets. The api_level
must match the API of the standalone NDK toolchain that NDK_HOME
points at.
